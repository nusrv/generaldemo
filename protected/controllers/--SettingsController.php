<?php

class SettingsController extends BaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','index','view','update_settings','edit'),
				'users'=>array(Yii::app()->user->getState('type')),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionEdit(){
		if(isset($_POST)){
			if(isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk']) && isset($_POST['scenario'])){
				$model = $this->loadModel($_POST['pk']);

				if(!empty($_POST['name'])){
					$name = $_POST['name'];
					$value = $_POST['value'];
					$model->$name = $value;
					if($model->save())
						echo true;
					else
						echo false;
				}
			}

		}
	}

	public function actionUpdate_settings($id){
		$model=$this->loadModel($id);


		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Settings']))
		{
			$model->attributes=$_POST['Settings'];
			$model->updated_at = date('Y-m-d H:i:S');
			if($model->save())


				$_POST['submitted'] = true;

		}

		$this->render('update_settings',array(
			'model'=>$model,
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Settings;

		// Uncomment the following line if AJAX validation is needed
	        $this->performAjaxValidation($model);

		if(isset($_POST['Settings']))
		{

			$model->attributes=$_POST['Settings'];
			$model->created_at = date('Y-m-d H:i:s');
			 $model->category_id = $_POST['Settings']['category_id'];

			if($model->save()) {

				$this->redirect(array('admin'));
			}}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);


		// Uncomment the following line if AJAX validation is needed
	 $this->performAjaxValidation($model);

		if(isset($_POST['Settings']))
		{
			$model->attributes=$_POST['Settings'];
			$model->updated_at = date('Y-m-d H:i:s');
			$model->timezone = $_POST['Settings']['timezone'];
			if($model->save())
			$_POST['submitted'] = true;

		}

		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Settings('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Settings']))
			$model->attributes=$_GET['Settings'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{


		$model_create=new Settings;

		$this->performAjaxValidationCustom($model_create);

		if(isset($_POST['Settings'])) {

			$post = $_POST['Settings'];

			$model_create->attributes =$post;

			$model_create->created_at = date('Y-m-d H:i:s');
			$model_create->direct_push_start = $post['direct_push_start'];
			$model_create->direct_push_end = $post['direct_push_end'];
			$model_create->direct_push = $post['direct_push'];
			$model_create->category_id = $post['category_id'];

				if ($model_create->save(true))
					$this->redirect(array('admin'));

		}

		/*$this->render('create',array(
			'model'=>$model,
		));*/
		$model_general = $this->loadModel(1);
		$model=new Settings('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Settings']))
			$model->attributes=$_GET['Settings'];

		$this->render('admin',array(
			'model'=>$model,
			'model_create'=>$model_create,
			'model_general'=>$model_general,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Settings the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Settings::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Settings $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='settings-form' )
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	protected function performAjaxValidationCustom($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='settings-custom-form' )
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
