<?php
class TestController extends BaseController
{
    private $start_time;

    private $end_time;

    private $direct_push_start;

    private $direct_push_end;

    private $is_custom_settings;

    private $is_direct_push;

    private $gap_time = 0;
    private $settings ;
    private $PlatFrom ;
    private $per_post_day ;





    public function actionIndex()
    {
        $this->settings = Settings::model()->findByPk(1);

        $this->PlatFrom = Platform::model()->findAllByAttributes(array('deleted' => 0));

        $this->per_post_day = $this->settings->how_many_posts;

        $this->start_time = $this->settings->start_date_time;

        $this->end_time = $this->settings->end_date_time;

        $this->gap_time = $this->settings->gap_time;

        $this->direct_push_start = $this->settings->direct_push_start;

        $this->direct_push_end = $this->settings->direct_push_end;

        $this->is_custom_settings = false;

        $this->is_direct_push = $this->settings->direct_push;

        echo '<pre>';

        foreach ($this->PlatFrom as $item) {

            print_r($this->date(7,$item->id));
        }
    }


    private function custom_settings($times,$platform_id){

        $this->is_custom_settings =true;

        $condition = new CDbCriteria();

        $condition->order = 'id Desc';

        $condition->limit = 1;

        $condition->condition = 'is_scheduled = 1 and settings="custom" and platform_id = '.$platform_id;

        $check = PostQueue::model()->find($condition);

        $final_date = strtotime(date('Y-m-d H:i:s'));

        $is_scheduled = 0;

        $obj = null ;



        foreach ($times as $timeItem) {

            $timeItem=(object)$timeItem;

            print_r($timeItem);


            $obj = $timeItem->obj_id;

            $start = strtotime(date('Y-m-d') . ' ' . $timeItem->start_time);

            $end = strtotime(date('Y-m-d') . ' ' . $timeItem->end_time);

            $now = strtotime(date('Y-m-d H:i:s') . ' + ' . $timeItem->gap_time . ' minutes');




            if ($end < $start) {
                $extra_day = 1;
                $end = strtotime(date('Y-m-d') . ' ' . $timeItem->end_time . ' + ' . $extra_day . ' day');
            }


            if ($timeItem->is_direct_push) {
                $direct_start = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_start_time);
                $direct_end = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_end_time);

                if ($direct_end < $direct_start) {
                    $extra_day = 1;
                    $direct_end = strtotime(date('Y-m-d') . ' ' . $timeItem->direct_push_end_time . ' + ' . $extra_day . ' day');
                }
                $now = strtotime(date('Y-m-d H:i:s'));
                if (($now >= $direct_start) && ($now <= $direct_end)) {
                     $final_date = strtotime(date('Y-m-d H:i:s') . ' - 1 minute');
                    $is_scheduled = 1;
                    break;
                }

            }




            if (empty($check)) {
                if (($now <= $start)) {
                    $final_date = $start;
                    $is_scheduled = 1;

                } elseif ($now <= $end) {
                    $final_date = $now;
                    $is_scheduled = 1;

                }
            } else {
                $date = strtotime($check->schedule_date . ' + ' . $timeItem->gap_time . ' minutes');
                if ($date < $now) {
                    $date = $now;
                }

                if (($date <= $start)) {
                    $final_date = $start;
                    $is_scheduled = 1;

                } elseif ($date <= $end) {
                    $final_date = $date;
                    $is_scheduled = 1;

                }
            }



        }


         $post_date = date('Y-m-d H:i:s', $final_date);


        return array($post_date, $is_scheduled ,$obj);

    }


    private function date($cat_id,$platform_id)
    {

        $times=  Yii::app()->db->createCommand("                   SELECT
                platform_category_settings.obj_id,
                platform_category_settings.platform_id,
                time_settings.start_time,
                time_settings.end_time,
                time_settings.is_direct_push,
                time_settings.direct_push_start_time,
                time_settings.direct_push_end_time,
                time_settings.gap_time
                FROM platform_category_settings
                
                INNER JOIN days_settings
                ON days_settings.platform_category_settings_id =platform_category_settings.id
                
                INNER JOIN day
                ON days_settings.day_id =day.id
                
                INNER JOIN time_settings
                ON time_settings.platform_category_settings_id =platform_category_settings.id
                INNER JOIN settings_obj
                ON settings_obj.id =platform_category_settings.obj_id
                
                 WHERE 
                 platform_category_settings.category_id = ".$cat_id." 
                 and 
                 platform_category_settings.platform_id = ".$platform_id."
                 and
                 day.title = DAYNAME(NOW())
                 and 
                 settings_obj.active = 1
        ")->queryAll();




        if(!empty($times)){


           list($time,$is_scheduled ,$obj) =  $this->custom_settings($times,$platform_id);


           return array($time,$is_scheduled ,$obj);

        }



        $final_date = date('Y-m-d H:i:s');


        $condition = new CDbCriteria();

        $condition->order = 'id Desc';

        $condition->limit = 1;

        $condition->condition = 'is_scheduled = 1 and settings="general"';


        $check = PostQueue::model()->find($condition);
        $is_scheduled = 1;
        $now = strtotime(date('Y-m-d H:i:s') . ' + ' . $this->gap_time . ' minutes');
        $start = strtotime(date('Y-m-d') . ' ' . $this->start_time);
        $end = strtotime(date('Y-m-d') . ' ' . $this->end_time);

        if ($end < $start) {
            $extra_day = 1;
            $end = strtotime(date('Y-m-d') . ' ' . $this->end_time . ' + ' . $extra_day . ' day');
        }

        if (empty($check)) {
            if (($now <= $start)) {
                $final_date = $start;

            } elseif ($now <= $end) {
                $final_date = $now;
            } else {
                $final_date = $now;
                $is_scheduled = 0;
            }
        } else {
            $date = strtotime($check->schedule_date . ' + ' . $this->gap_time . ' minutes');
            if ($date < $now) {
                $date = $now;
            }

            if (($date <= $start)) {
                $final_date = $start;
            } elseif ($date <= $end) {
                $final_date = $date;
            } else {
                $final_date = $date;
                $is_scheduled = 0;
            }
        }

        if ($this->is_direct_push) {
            $direct_start = strtotime(date('Y-m-d') . ' ' . $this->direct_push_start);
            $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end);

            if ($direct_end < $direct_start) {
                $extra_day = 1;
                $direct_end = strtotime(date('Y-m-d') . ' ' . $this->direct_push_end . ' + ' . $extra_day . ' day');
            }
            $now = strtotime(date('Y-m-d H:i:s'));
            if (($now >= $direct_start) && ($now <= $direct_end)) {
                $final_date = strtotime(date('Y-m-d H:i:s') . ' -1 minute');
                $is_scheduled = 1;
            }
        }

        $post_date = date('Y-m-d H:i:s', $final_date);

        return array($post_date, $is_scheduled ,false);

    }

}
?>