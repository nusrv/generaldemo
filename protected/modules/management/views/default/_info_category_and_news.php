<?PHP
/* @var $category Category */
/* @var $this DefaultController */
/* @var $all_news DefaultController */
?>
<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Section Report</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <?PHP
                    $counter = 0;
                    $class = array(
                        'aqua',
                        'red',
                        'green',
                        'yellow',
                    );
                    foreach ($category as $item) {
                        $this->renderPartial('_info_category', array('data' => $item, 'total' => $all_news, 'color' => $class[$counter]));
                        $counter++;
                        if ($counter == 3)
                            $counter = 0;
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>
