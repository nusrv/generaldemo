<?PHP /* @var $this DefaultController */ ?>
 <!--   <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">CPU Traffic</span>
                <span class="info-box-number"><?PHP /*echo $server_cpu */?><small>%</small></span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-speedometer"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Memory</span>
                <span class="info-box-number"><?PHP /*echo $server_memory */?><small></small></span>
            </div>
        </div>
    </div>-->
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><?PHP echo TbHtml::link('Users',$this->createUrl('user/admin'))?></span>
                <span class="info-box-number"><?PHP echo User::model()->get_number_user() ?></span>
            </div>
        </div>
    </div>