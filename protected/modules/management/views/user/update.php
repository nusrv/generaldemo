<?php
/* @var $this UserController */
/* @var $model User */

$this->pageTitle = "Users | Update";

$this->breadcrumbs=array(
	'Management'=>array('/'.$this->module->id),
	'Users'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);


?>

	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<div class="col-sm-9"><h2>        <?php $this->widget(
									'booster.widgets.TbButtonGroup',
									array(
										'size' => 'small',
										'context' => 'info',
										'buttons' => array(
											array(
												'label' => 'Update password',
												'buttonType' => 'link',
												'url' => array('/management/user/updatePassword/', 'id' => $model->id)
											),
										),
									)
								); ?></h2></div>
						<div class="col-sm-3" style="padding-top: 19px;text-align: left;">
							<div class="col-sm-7"><?php echo Yii::app()->params['statement']['previousPage']; ?></div>

							<div class="col-sm-5">
								<?PHP
								$this->widget(
									'booster.widgets.TbButtonGroup',
									array(
										'size' => 'small',
										'context' => 'info',
										'buttons' => array(
											array(
												'label' => 'Manage',
												'buttonType' => 'link',
												'url' => array('user/admin')
											),
										),
										/*'htmlOptions'=>array(
                                            'class'=>'pull-right	'
                                        )*/
									)
								);

								?>
							</div>
						</div>
					</div>
					<div class="box-body">
						<?php $this->renderPartial('_form', array('model' => $model)); ?>
					</div>
				</div>
			</div>
	</section>
