<?php

class DefaultController extends BaseController
{
/*    public function actionTotal_posts(){
        $model = new PostQueue();
        if(isset($_GET['PostQueue'])){
            Yii::app()->session['total_from'] = date('Y-m-d',strtotime($_GET['PostQueue']['schedule_date']));
            Yii::app()->session['total_to'] = date('Y-m-d',strtotime($_GET['PostQueue']['to']));

        }

        if(isset(Yii::app()->session['total_from'])){
            $from = Yii::app()->session['total_from'];

        } else{
            $from = date('Y-m-d');
        }
        if(isset(Yii::app()->session['total_to'])){
            $to = Yii::app()->session['total_to'];
        }else{
            $to = date('Y-m-d');
        }
        $model->schedule_date = $from;
        $model->to = $to;

        $total_posts['all_posts_facebook'] =  PostQueue::model()->total_post('Facebook','all',$from,$to);
        $total_posts['all_posts_twitter'] =  PostQueue::model()->total_post('Twitter','all',$from,$to);
        $total_posts['all_posts_instagram'] =  PostQueue::model()->total_post('Instagram','all',$from,$to);
        $total_posts['total_all_posts_instagram'] = $total_posts['all_posts_facebook'] + $total_posts['all_posts_twitter'] + $total_posts['all_posts_instagram'];

        $total_posts['thematic_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to);
        $total_posts['thematic_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to);
        $total_posts['thematic_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to);
        $total_posts['total_thematic'] = $total_posts['thematic_facebook'] + $total_posts['thematic_twitter'] +$total_posts['thematic_instagram'];

        $total_posts['thematic_image_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to,false,'Image');
        $total_posts['thematic_image_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to,false,'Image');
        $total_posts['thematic_image_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to,false,'Image');
        $total_posts['total_thematic_image'] = $total_posts['thematic_image_facebook']+$total_posts['thematic_image_twitter']+ $total_posts['thematic_image_instagram'];

        $total_posts['thematic_preview_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to,false,'Preview');
        $total_posts['thematic_preview_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to,false,'Preview');
        $total_posts['thematic_preview_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to,false,'Preview');
        $total_posts['total_thematic_preview'] = $total_posts['thematic_preview_facebook']+ $total_posts['thematic_preview_twitter'] +$total_posts['thematic_preview_instagram'];

        $total_posts['thematic_text_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to,false,'Text');
        $total_posts['thematic_text_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to,false,'Text');
        $total_posts['thematic_text_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to,false,'Text');
        $total_posts['total_thematic_text'] = $total_posts['thematic_text_facebook']+$total_posts['thematic_text_twitter']+ $total_posts['thematic_text_instagram'];


        $total_posts['thematic_video_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to,false,'Video');
        $total_posts['thematic_video_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to,false,'Video');
        $total_posts['thematic_video_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to,false,'Video');
        $total_posts['total_thematic_video'] = $total_posts['thematic_video_facebook']+$total_posts['thematic_video_twitter']+ $total_posts['thematic_video_instagram'];

        $total_posts['nonthematic_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to);
        $total_posts['nonthematic_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to);
        $total_posts['nonthematic_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to);
        $total_posts['total_nonthematic'] = $total_posts['nonthematic_facebook'] +$total_posts['nonthematic_twitter'] +$total_posts['nonthematic_instagram'];

        $total_posts['nonthematic_image_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to,false,'Image');
        $total_posts['nonthematic_image_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to,false,'Image');
        $total_posts['nonthematic_image_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to,false,'Image');
        $total_posts['total_nonthematic_image'] = $total_posts['nonthematic_image_facebook']+$total_posts['nonthematic_image_twitter']+ $total_posts['nonthematic_image_instagram'];

        $total_posts['nonthematic_preview_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to,false,'Preview');
        $total_posts['nonthematic_preview_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to,false,'Preview');
        $total_posts['nonthematic_preview_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to,false,'Preview');
        $total_posts['total_nonthematic_preview'] = $total_posts['nonthematic_preview_facebook']+$total_posts['nonthematic_preview_twitter']+ $total_posts['nonthematic_preview_instagram'];

        $total_posts['nonthematic_text_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to,false,'Text');
        $total_posts['nonthematic_text_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to,false,'Text');
        $total_posts['nonthematic_text_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to,false,'Text');
        $total_posts['total_nonthematic_text'] = $total_posts['nonthematic_text_facebook']+$total_posts['nonthematic_text_twitter']+ $total_posts['nonthematic_text_instagram'];

        $total_posts['nonthematic_video_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to,false,'Video');
        $total_posts['nonthematic_video_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to,false,'Video');
        $total_posts['nonthematic_video_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to,false,'Video');
        $total_posts['total_nonthematic_video'] = $total_posts['nonthematic_video_facebook']+$total_posts['nonthematic_video_twitter']+ $total_posts['nonthematic_video_instagram'];

        $total_posts['editedautomated_facebook'] = PostQueue::model()->total_post('Facebook','auto',$from,$to,true);
        $total_posts['editedautomated_twitter'] = PostQueue::model()->total_post('Twitter','auto',$from,$to,true);
        $total_posts['editedautomated_instagram'] = PostQueue::model()->total_post('Instagram','auto',$from,$to,true);
        $total_posts['total_editedautomated'] =$total_posts['editedautomated_facebook'] +$total_posts['editedautomated_twitter'] +$total_posts['editedautomated_instagram'];

        $total_posts['withouteditautomated_facebook'] = PostQueue::model()->total_post('Facebook','auto',$from,$to);
        $total_posts['withouteditautomated_twitter'] = PostQueue::model()->total_post('Twitter','auto',$from,$to);
        $total_posts['withouteditautomated_instagram'] = PostQueue::model()->total_post('Instagram','auto',$from,$to);
        $total_posts['total_withouteditautomated'] =$total_posts['withouteditautomated_facebook'] +$total_posts['withouteditautomated_twitter'] +$total_posts['withouteditautomated_instagram'];

        $total_posts['automated_facebook'] = $total_posts['editedautomated_facebook'] + $total_posts['withouteditautomated_facebook'];
        $total_posts['automated_twitter'] = $total_posts['editedautomated_twitter'] + $total_posts['withouteditautomated_twitter'];
        $total_posts['automated_instagram'] = $total_posts['editedautomated_instagram']+ $total_posts['withouteditautomated_instagram'];
        $total_posts['total_automated'] = $total_posts['automated_facebook']  + $total_posts['automated_twitter'] +$total_posts['automated_instagram'];

        $total_posts['cloned_facebook'] = PostQueue::model()->total_post('Facebook','clone',$from,$to);
        $total_posts['cloned_twitter'] = PostQueue::model()->total_post('Twitter','clone',$from,$to);
        $total_posts['cloned_instagram'] = PostQueue::model()->total_post('Instagram','clone',$from,$to);
        $total_posts['total_cloned'] = $total_posts['cloned_facebook']+$total_posts['cloned_twitter'] +$total_posts['cloned_instagram'];


        $this->render('total_posts',array('total_posts'=>$total_posts,'model'=>$model));

    }*/

	public function actionIndex()
	{
        $model = new PostQueue();
        if(isset($_GET['PostQueue'])){
            Yii::app()->session['total_from'] = date('Y-m-d',strtotime($_GET['PostQueue']['schedule_date']));
            Yii::app()->session['total_to'] = date('Y-m-d',strtotime($_GET['PostQueue']['to']));

        }

        if(isset(Yii::app()->session['total_from'])){
            $from = Yii::app()->session['total_from'];

        } else{
            $from = date('Y-m-d');
        }
        if(isset(Yii::app()->session['total_to'])){
            $to = Yii::app()->session['total_to'];
        }else{
            $to = date('Y-m-d');
        }
        $model->schedule_date = $from;
        $model->to = $to;

        $total_posts['all_posts_facebook'] =  PostQueue::model()->total_post('Facebook','all',$from,$to);
        $total_posts['all_posts_twitter'] =  PostQueue::model()->total_post('Twitter','all',$from,$to);
        $total_posts['all_posts_instagram'] =  PostQueue::model()->total_post('Instagram','all',$from,$to);
        $total_posts['total_all_posts_instagram'] = $total_posts['all_posts_facebook'] + $total_posts['all_posts_twitter'] + $total_posts['all_posts_instagram'];

        $total_posts['thematic_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to);
        $total_posts['thematic_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to);
        $total_posts['thematic_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to);
        $total_posts['total_thematic'] = $total_posts['thematic_facebook'] + $total_posts['thematic_twitter'] +$total_posts['thematic_instagram'];

        $total_posts['thematic_image_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to,false,'Image');
        $total_posts['thematic_image_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to,false,'Image');
        $total_posts['thematic_image_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to,false,'Image');
        $total_posts['total_thematic_image'] = $total_posts['thematic_image_facebook']+$total_posts['thematic_image_twitter']+ $total_posts['thematic_image_instagram'];

        $total_posts['thematic_preview_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to,false,'Preview');
        $total_posts['thematic_preview_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to,false,'Preview');
        $total_posts['thematic_preview_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to,false,'Preview');
        $total_posts['total_thematic_preview'] = $total_posts['thematic_preview_facebook']+ $total_posts['thematic_preview_twitter'] +$total_posts['thematic_preview_instagram'];

        $total_posts['thematic_text_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to,false,'Text');
        $total_posts['thematic_text_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to,false,'Text');
        $total_posts['thematic_text_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to,false,'Text');
        $total_posts['total_thematic_text'] = $total_posts['thematic_text_facebook']+$total_posts['thematic_text_twitter']+ $total_posts['thematic_text_instagram'];


        $total_posts['thematic_video_facebook'] = PostQueue::model()->total_post('Facebook','thematic',$from,$to,false,'Video');
        $total_posts['thematic_video_twitter'] = PostQueue::model()->total_post('Twitter','thematic',$from,$to,false,'Video');
        $total_posts['thematic_video_instagram'] = PostQueue::model()->total_post('Instagram','thematic',$from,$to,false,'Video');
        $total_posts['total_thematic_video'] = $total_posts['thematic_video_facebook']+$total_posts['thematic_video_twitter']+ $total_posts['thematic_video_instagram'];

        $total_posts['nonthematic_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to);
        $total_posts['nonthematic_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to);
        $total_posts['nonthematic_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to);
        $total_posts['total_nonthematic'] = $total_posts['nonthematic_facebook'] +$total_posts['nonthematic_twitter'] +$total_posts['nonthematic_instagram'];

        $total_posts['nonthematic_image_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to,false,'Image');
        $total_posts['nonthematic_image_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to,false,'Image');
        $total_posts['nonthematic_image_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to,false,'Image');
        $total_posts['total_nonthematic_image'] = $total_posts['nonthematic_image_facebook']+$total_posts['nonthematic_image_twitter']+ $total_posts['nonthematic_image_instagram'];

        $total_posts['nonthematic_preview_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to,false,'Preview');
        $total_posts['nonthematic_preview_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to,false,'Preview');
        $total_posts['nonthematic_preview_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to,false,'Preview');
        $total_posts['total_nonthematic_preview'] = $total_posts['nonthematic_preview_facebook']+$total_posts['nonthematic_preview_twitter']+ $total_posts['nonthematic_preview_instagram'];

        $total_posts['nonthematic_text_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to,false,'Text');
        $total_posts['nonthematic_text_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to,false,'Text');
        $total_posts['nonthematic_text_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to,false,'Text');
        $total_posts['total_nonthematic_text'] = $total_posts['nonthematic_text_facebook']+$total_posts['nonthematic_text_twitter']+ $total_posts['nonthematic_text_instagram'];

        $total_posts['nonthematic_video_facebook'] = PostQueue::model()->total_post('Facebook','manual',$from,$to,false,'Video');
        $total_posts['nonthematic_video_twitter'] = PostQueue::model()->total_post('Twitter','manual',$from,$to,false,'Video');
        $total_posts['nonthematic_video_instagram'] = PostQueue::model()->total_post('Instagram','manual',$from,$to,false,'Video');
        $total_posts['total_nonthematic_video'] = $total_posts['nonthematic_video_facebook']+$total_posts['nonthematic_video_twitter']+ $total_posts['nonthematic_video_instagram'];

        $total_posts['editedautomated_facebook'] = PostQueue::model()->total_post('Facebook','auto',$from,$to,true);
        $total_posts['editedautomated_twitter'] = PostQueue::model()->total_post('Twitter','auto',$from,$to,true);
        $total_posts['editedautomated_instagram'] = PostQueue::model()->total_post('Instagram','auto',$from,$to,true);
        $total_posts['total_editedautomated'] =$total_posts['editedautomated_facebook'] +$total_posts['editedautomated_twitter'] +$total_posts['editedautomated_instagram'];

        $total_posts['withouteditautomated_facebook'] = PostQueue::model()->total_post('Facebook','auto',$from,$to);
        $total_posts['withouteditautomated_twitter'] = PostQueue::model()->total_post('Twitter','auto',$from,$to);
        $total_posts['withouteditautomated_instagram'] = PostQueue::model()->total_post('Instagram','auto',$from,$to);
        $total_posts['total_withouteditautomated'] =$total_posts['withouteditautomated_facebook'] +$total_posts['withouteditautomated_twitter'] +$total_posts['withouteditautomated_instagram'];

        $total_posts['automated_facebook'] = $total_posts['editedautomated_facebook'] + $total_posts['withouteditautomated_facebook'];
        $total_posts['automated_twitter'] = $total_posts['editedautomated_twitter'] + $total_posts['withouteditautomated_twitter'];
        $total_posts['automated_instagram'] = $total_posts['editedautomated_instagram']+ $total_posts['withouteditautomated_instagram'];
        $total_posts['total_automated'] = $total_posts['automated_facebook']  + $total_posts['automated_twitter'] +$total_posts['automated_instagram'];

        $total_posts['cloned_facebook'] = PostQueue::model()->total_post('Facebook','clone',$from,$to);
        $total_posts['cloned_twitter'] = PostQueue::model()->total_post('Twitter','clone',$from,$to);
        $total_posts['cloned_instagram'] = PostQueue::model()->total_post('Instagram','clone',$from,$to);
        $total_posts['total_cloned'] = $total_posts['cloned_facebook']+$total_posts['cloned_twitter'] +$total_posts['cloned_instagram'];

		$this->render('index',array(
			'server_memory'=>$this->convert(memory_get_usage(true)),
			'server_cpu'=>$this->get_server_cpu_usage(),
			'category'=>Category::model()->get_category(),
			'all_news'=>News::model()->get_all_news(),
            'total_posts'=>$total_posts,'model'=>$model
		));
	}

	function convert($size)
	{
		$unit=array('b','kb','Mb','gb','tb','pb');
		return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}

	function get_server_cpu_usage(){

		$load = sys_getloadavg();
		return $load[0];

	}

}