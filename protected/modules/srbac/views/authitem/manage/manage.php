<?php
$this->breadcrumbs = array('User Manage');
$this->pageTitle = 'Dashboard : user ';
$this->renderPartial("frontpage");
?>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <div class="col-sm-12">
                        <?PHP if($this->module->getMessage() != ""){ ?>
                            <div id="alert alert-danger">
                                <?php echo $this->module->getMessage();?>
                            </div>
                        <?php }?>
                    </div>
                    <div class="col-sm-4 text-center">


                        <?PHP
                        if(!$full){
                            if($this->module->getShowHeader()) {
                                $this->renderPartial($this->module->header);
                            }

                            ?>

                                <?php echo SHtml::ajaxLink('Manage AuthItem',
                                    array('manage','full'=>true),
                                    array(
                                        'type'=>'POST',
                                        'update'=>'#wizard',
                                        'beforeSend' => 'function(){
                                      $("#wizard").addClass("srbacLoading");
                                  }',
                                        'complete' => 'function(){
                                      $("#wizard").removeClass("srbacLoading");
                                  }',
                                    ),
                                    array(
                                        'name'=>'buttonManage',
                                        'onclick'=>"$(this).css('font-weight', 'bold');$(this).siblings().css('font-weight', 'normal');",
                                    )
                                );
                                ?>
                                <?php echo SHtml::ajaxLink('Autocreate Auth Items',
                                    array('auto'),
                                    array(
                                        'type'=>'POST',
                                        'update'=>'#wizard',
                                        'beforeSend' => 'function(){
                                      $("#wizard").addClass("srbacLoading");
                                  }',
                                        'complete' => 'function(){
                                      $("#wizard").removeClass("srbacLoading");
                                  }',
                                    ),
                                    array(
                                        'name'=>'buttonAuto',
                                        //'onclick'=>"$(this).css('font-weight', 'bold');$(this).siblings().css('font-weight', 'normal');",
                                    )
                                );
                                ?>
                                <?php echo SHtml::ajaxLink(
                                    SHtml::image($this->module->getIconsPath().'/allow.png',
                                        Helper::translate('srbac','Edit always allowed list'),
                                        array('class'=>'icon',
                                            'title'=>Helper::translate('srbac','Edit always allowed list'),
                                            'border'=>0
                                        )
                                    )." " .
                                    ($this->module->iconText ?
                                        Helper::translate('srbac','Edit always allowed list') :
                                        ""),
                                    array('editAllowed'),
                                    array(
                                        'type'=>'POST',
                                        'update'=>'#wizard',
                                        'beforeSend' => 'function(){
                                      $("#wizard").addClass("srbacLoading");
                                  }',
                                        'complete' => 'function(){
                                      $("#wizard").removeClass("srbacLoading");
                                  }',
                                    ),
                                    array(
                                        'name'=>'buttonAllowed',
                                        'onclick'=>"$(this).css('font-weight', 'bold');$(this).siblings().css('font-weight', 'normal');",
                                    )
                                );
                                ?>
                                <?php echo SHtml::ajaxLink(
                                    SHtml::image($this->module->getIconsPath().'/eraser.png',
                                        Helper::translate('srbac','Clear obsolete authItems'),
                                        array('class'=>'icon',
                                            'title'=>Helper::translate('srbac','Clear obsolete authItems'),
                                            'border'=>0
                                        )
                                    )." " .
                                    ($this->module->iconText ?
                                        Helper::translate('srbac','Clear obsolete authItems') :
                                        ""),
                                    array('clearObsolete'),
                                    array(
                                        'type'=>'POST',
                                        'update'=>'#wizard',
                                        'beforeSend' => 'function(){
                                      $("#wizard").addClass("srbacLoading");
                                  }',
                                        'complete' => 'function(){
                                      $("#wizard").removeClass("srbacLoading");
                                  }',
                                    ),
                                    array(
                                        'name'=>'buttonClear',
                                        'onclick'=>"$(this).css('font-weight', 'bold');$(this).siblings().css('font-weight', 'normal');",
                                    )
                                );
                                ?>

                        <?php } ?>

                    </div>
                    <div class="col-sm-8 text-center"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="wizard">
    <table class="srbacDataGrid" align="center">
        <tr>
            <th width="50%"><?php echo Helper::translate("srbac","Auth items");?></th>
            <th><?php echo Helper::translate('srbac','Actions')?></th>
        </tr>
        <tr>
            <td style="vertical-align: top;text-align: center">
                <div id="list">
                    <?php echo $this->renderPartial('manage/list', array(
                        'models'=>$models,
                        'pages'=>$pages,
                        'sort'=>$sort,
                    )); ?>
                </div>
            </td>
            <td style="vertical-align: top;text-align: center">
                <div id="preview">

                </div>
            </td>
        </tr>
    </table>
</div>
<?php if(!$full) {
  if($this->module->getShowFooter()) {
    $this->renderPartial($this->module->footer);
  }
}?>
