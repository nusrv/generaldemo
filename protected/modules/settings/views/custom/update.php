<?php
/* @var $this CustomController */
/* @var $days DaySettings */
/* @var $model CustomSettingsForm */
/* @var $obj SettingsObj*/

$this->breadcrumbs=array(
    'Custom'=>array('settings'),
    'Manage',
);
?>
<style>
    .checkbox{
        display: inline-block;
    }
    .select2-container-multi .select2-choices .select2-search-choice {
        font-size: 12px;
    }
    .margin_pules,.margin_remove{
        margin-top: 24px;
    }
</style>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="col-sm-9"></div>
                    <div class="col-sm-3" style=" text-align: left;">
                        <?php echo TbHtml::link('General Settings',$this->createUrl('/settings'),array('class'=>'btn btn-success btn-sm')) ?>
                        <?php echo Yii::app()->params['statement']['previousPage']; ?>
                    </div>
                </div>
                <div class="box-body">
                    <?php
                    foreach(Yii::app()->user->getFlashes() as $key => $message) {
                        echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
                    }
                    ?>
                    <?php
                     $this->renderPartial('_form_update',array('this'=>$this,'days'=>$days,'model'=>$model,'obj'=>$obj))?>
                </div>
            </div>
        </div>
    </div>
</section>