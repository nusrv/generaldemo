<?php
// This is the database connection configuration.
return array(
    // this is used in contact page
    'adminEmail'=>'webmaster@example.com',
    'webroot'   => '/var/www/html/generaldemo', // change this on production server
    'domain'   => 'http://staging.sortechs.com/generaldemo/', // change this on production server
    'amazonBucket' =>'sortechs',
    'amazonAccessKey' => 'AKIAJUNLV2HM7F4JO22A',
    'amazonSecretKey' => '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',
    'amazonFolder' => 'generaldemo',
    'amazonURL' => 'https://s3.amazonaws.com/',
    'feedUrl' => 'http://www.thenational.ae',
    'feedUrlRss' => 'http://www.thenational.ae/section/rss',
    'HashTagRoot'=>array(
        'ar'=>'#ناشونال',
        'en'=>'#thenational'
    ),
    'line'=>'بقلم',
    'source'=>'المصدر',

    /*gmail
sortechs.demo@gmail.com
Demo@123*/
    //Facebook config
    'facebook'=>array(
        'LinkPage'=>'https://www.facebook.com/Sortechs_GeneralDemo-334767050206167/',
        'page_id'=>'334767050206167', // Your facebook Page ID
        'app_id'=>'985854914794106', // Your facebook app ID
        'secret'=>'049117c99fc665419e3301ba81b96bfe', // Your facebook secret
        'token'=>'CAAOAoTvZCEnoBAFarzGvRdDjG03ouNkiK5NYyj7aO3XvRTmGZAUrFPNrD3RWkVfV12iNpgjUMvZBHf0D1JsuKJNawjsZBOHM1FPw26uOoBMOgvZBEewy7ygpzQZCVZCAl4rn3cOk1FP5hS5uXzZByzVX57WdndvqPi3x8IvWmUp8vwAgLh7zNle5rc480F2tZCkgyAaCBUe4UcwZDZD', // The access token you receieved above

    ),

    //Instagram config
    'instagram_auth'=>array(
        'LinkPage'=>'https://www.instagram.com/general_demo/',
        'username' => 'General_demo',
        'password' => 'sortechs@123',
        'debug'    => true
    ),

    //twitter config

    'twitter'=>array(
        'LinkPage'=>'https://twitter.com/SortechsDemo',
        'key'=>'g1ElOuWndWPHnlNpSZU6PkV1t',
        'secret'=>'i3RbhFseBjl05obzqSpthxPhUVUrenBMOSjtLo9snWDABxkXV8',
        'token'=>'782941674179858433-E83vgVouhbyYGSGibU02ReVubffdcSF',
        'token_secret'=>'Tk2S9EmHX8X4zGdwQO9l19cdp5l5JU7gutcICrW7AMToO',
    ),

    'templates'=>array(
        'text'=>'[title]'.PHP_EOL.'[short_link]'.PHP_EOL.'[section]',
        'type'=>'Preview',
    ),

    'rules'=>array(
        'facebook' =>array(
            'Preview',
            'Preview',
            /*'text',*/
            'Preview',
            'Preview',
            'Preview',
            'Preview',
            'Preview',
            'Preview',

            /*'image',
            'image',
            'image',*/
            /*'text',*/
            'image',
            'image',
            //'video',
            // 'Multiple_Image',
            //  'Albums',
            // 'Carousel',
        ),
        'twitter'=>array(

            'Preview',
            'Preview',
            /*'text',*/
            'Preview',
            'Preview',
            'Preview',
            'Preview',
            'Preview',
            'Preview',

            /*'image',
            'image',
            'image',*/
            /*'text',*/
            'image',
            'image',
            //'video',
            // 'Multiple_Image',
            //  'Albums',
            // 'Carousel',


        ),
        'instagram'=> array(
            'image',
            // 'video',
        )
    ),

    'rules_hashTag'=>'main',
    'other_hahTag'=>'hash_tag',

    'shorten_url'=>'للمزيد من التفاصيل :',

    'rule_array'=>array(
        'facebook' =>array(
            'text'=>'text',
            'preview'=>'preview',
            'image'=>'image',
            'video'=>'video',
            'youtube'=>'youtube',
            // 'Multiple_Image',
            //  'Albums',
            // 'Carousel',
        ),
        'twitter'=>array(
            'text'=>'text',
            'preview'=>'preview',
            'image'=>'image',
            'video'=>'video',
            'youtube'=>'youtube',
        ),
        'instagram'=> array(
            'image'=>'image',
        )
    ),

    'rule_template'=>array(
        'facebook' =>array(
            'image'=>'image',
            'text'=>'text',
            'preview'=>'preview',

            'video'=>'video',
            // 'Multiple_Image',
            //  'Albums',
            // 'Carousel',
        ),
        'twitter'=>array(
            'image'=>'image',
            'text'=>'text',
            'preview'=>'preview',

            'video'=>'video',
        ),
        'instagram'=> array(
            'image'=>'image',
        )
    ),

    'rule_filter'=>array(
        ''=>'',
        'text'=>'text',
        'preview'=>'preview',
        'image'=>'image',
        'video'=>'video',
    ),

    'statement'=>array(
        'previousPage'=>CHtml::button('Previous Page',array('onClick'=>'history.go(-1)','class'=>'btn btn-primary btn-sm pull-right')),
    ),

    /*'email'=>array(
        'm.salahat@nusrv.com',
        'mkhader@nusrv.com'
    ),*/
);