<?php
/* @var $this PlatformController */
/* @var $model Platform */

$this->breadcrumbs=array(
	'Platforms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Platform', 'url'=>array('index')),
	array('label'=>'Manage Platform', 'url'=>array('admin')),
);
?>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-md-9"><h2>Platform || Create</h2></div>
					<div class="col-md-3" style="padding-top: 19px;text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>

						<?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
											array('label' => 'Create', 'url'=>array('create')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
								),
								'htmlOptions'=>array(
									'class'=>'pull-right	'
								)
							)
						);

						?>
					</div>
				</div>
				<div class="box-body">
					<?php $this->renderPartial('_form', array('model'=>$model)); ?>
				</div>
			</div>
		</div>
</section>