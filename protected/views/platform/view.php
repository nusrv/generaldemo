<?php
/* @var $this PlatformController */
/* @var $model Platform */

$this->breadcrumbs=array(
	'Platforms'=>array('index'),
	$model->title,
);

?>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<div class="col-md-9"><h2>Platform || <?PHP echo $model->title?></h2></div>
					<div class="col-md-3" style="padding-top: 19px;text-align: left;">
						<?php echo Yii::app()->params['statement']['previousPage']; ?>

						<?PHP
						$this->widget(
							'booster.widgets.TbButtonGroup',
							array(
								'size' => 'small',
								'context' => 'info',
								'buttons' => array(
									array(
										'label' => 'Action',
										'items' => array(
											array('label' => 'Create', 'url'=>array('create')),
											array('label' => 'Update', 'url'=>array('update', 'id'=>$model->id)),
											array('label' => 'Delete', 'url' => '#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
											array('label' => 'Manage', 'url'=>array('admin'))
										)
									),
								),
								'htmlOptions'=>array(
									'class'=>'pull-right	'
								)
							)
						);

						?>
					</div>
				</div>
				<div class="box-body">
					<?php $this->widget('booster.widgets.TbDetailView', array(
						'data'=>$model,
						'attributes'=>array(
							'id',
							'title',

							'created_at',
						),
					)); ?>
				</div>
			</div>
		</div>
</section>

