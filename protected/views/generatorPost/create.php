<?php
/* @var $this GeneratorPostController */
/* @var $model GeneratorPost */

$this->pageTitle ='Generate post'
?>
<script>
	$(document).ready(function(){
		$("#GeneratorPost_submit_to_all").click(function(){
			if($(this).is(':checked')){
				$('.Platforms').prop('disabled',true);
			}else{
				$('.Platforms').prop('disabled',false);
			}
		});
		$('#GeneratorPost_time').datetimepicker({
			format:'Y-m-d H:i',step:5,
			minDate: new Date('Y-m-d H:i')
		});

	})
</script>
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-info">
					<div class="box-header with-border">
							<div class="col-xs-12 pull-right"><?php echo Yii::app()->params['statement']['previousPage']; ?></div>
					</div>
					<div class="box-body">
						<?php $this->renderPartial('_form', array('model' => $model)); ?>
					</div>
				</div>
			</div>
	</section>