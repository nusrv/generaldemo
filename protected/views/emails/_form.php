<?php
/* @var $this EmailsController */
/* @var $model Emails */
/* @var $form TbActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'emails-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'type' => 'horizontal',

)); ?>

	<div class="col-sm-3">
		<?php
		echo $form->textFieldGroup($model,'f_name',array(
			'labelOptions'=>array(
				'class'=>'col-sm-3'
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-9',
			),
		));
		?>
	</div>
	<div class="col-sm-3">
		<?php
		echo $form->textFieldGroup($model,'l_name',array(
			'labelOptions'=>array(
				'class'=>'col-sm-3'
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-9',
			),
		) );
		?>
		</div>
		<div class="col-sm-3">	<?php
		echo $form->emailFieldGroup($model,'email',array(
			'labelOptions'=>array(
				'class'=>'col-sm-3'
			),
			'wrapperHtmlOptions' => array(
				'class' => 'col-sm-9',
			),
		) );
		?>
	</div>
    <div class="form-actions  col-sm-3"  >
        <?php $this->widget(
            'booster.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'context' => 'success',
                'label' => $model->isNewRecord ? 'Add New' : 'Save'
            )
        ); ?>

    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->