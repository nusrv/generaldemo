<?php
/* @var $form TbActiveForm */
$form=$this->beginWidget('booster.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'form-visible',
)); ?>
    <div class="col-sm-2 ">
        <?php echo $form->textFieldGroup(
            $model,
            'id',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_id',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(

                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_id?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>

    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'f_name',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_f_name',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(

                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_f_name?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'l_name',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_l_name',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                               // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                     'htmlOptions'=>array(

                        'disabled'=>$model->visible_l_name?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>
    <div class="col-sm-2">
        <?php echo $form->textFieldGroup(
            $model,
            'email',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_email',
                    array(
                        'inline'=>true,
                        //'labelOptions'=>array('class'=>'btn btn-primary'),
                        'widgetOptions'=>array(
                            'htmlOptions'=>array(
                                // 'style'=>'opacity: 0;'
                            ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'htmlOptions'=>array(

                        'disabled'=>$model->visible_email?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>

    <div class="col-sm-2">
        <?php echo $form->datePickerGroup(
            $model,
            'created_at',
            array(
                'prepend' => $form->checkboxGroup(
                    $model,
                    'visible_created_at',
                    array(
                        'inline'=>true,
                         'widgetOptions'=>array(
                            'htmlOptions'=>array(
                             ),
                        ),
                    )
                ),
                'widgetOptions'=>array(
                    'options' => array(
                        'language' => 'en',
                        'format' => 'yyyy-mm-dd',
                        'viewformat' => 'yyyy-mm-dd',
                    ),

                    'htmlOptions'=>array(
                        'disabled'=>$model->visible_created_at?false:true
                    )
                ),
                'hint'=>''
            )
        ); ?>

    </div>

    <div class="col-sm-2 pull-left page-sizes"  >

        <?php echo $form->dropDownListGroup(
            $model,
            'pagination_size',
            array(

                'widgetOptions'=>array(
                    'data'=>$model->pages_size(),
                    'htmlOptions'=>array(

                    ),
                ),
                'hint'=>''
            )
        ); ?>
    </div>



<?php $this->endWidget(); ?>