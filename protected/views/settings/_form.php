<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form TbActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'settings-form',

    // Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'action'=>$this->createUrl('settings/update/'.$model->id),
	'enableAjaxValidation'=>true,
)); ?>
	<style>
		#yw0{
			margin-right:10px;
		}
	</style>

	<div class="col-sm-12">

		<div class="col-sm-12"><h3 style="color:darkred">Posts Configurations</h3></div>

		<div class="col-sm-6">
			<a href="#"  onclick="guiders.show('firstSettings');return false"    ><i class="fa fa-question-circle"></i></a>

			<?php
			echo $form->dropDownListGroup($model,'how_many_posts',array(
				'widgetOptions'=>array(
					'data'=>$model->all_numbers(),

				),

			));
			?>
		</div>

		<div class="col-sm-6">
			<a href="#"  onclick="guiders.show('second');return false"    ><i class="fa fa-question-circle"></i></a>

			<?php
			echo $form->dropDownListGroup($model,'gap_time',array(
				'widgetOptions'=>array(
					'data'=>$model->gap_time(),

				),

			));
			?>
		</div>


		<div class="col-sm-6">
			<a href="#"  onclick="guiders.show('third');return false"    ><i class="fa fa-question-circle"></i></a>

			<?php
			echo $form->timeFieldGroup($model,'start_date_time',array(
				'widgetOptions'=>array(
					'htmlOptions'=>array('class'=>'StartTime')
				)
			));
			?>
		</div>
		<div class="col-sm-6">
			<a href="#"  onclick="guiders.show('fourth');return false"    ><i class="fa fa-question-circle"></i></a>

			<?php
			echo $form->timeFieldGroup($model,'end_date_time',array(
				'widgetOptions'=>array(

					'htmlOptions'=>array('class'=>'EndTime')
				)
			));
			?>
		</div>

		<div class="col-sm-6">
			<?php
			echo $form->dropDownListGroup($model,'timezone',array(
				'widgetOptions'=>array(
					'data'=>$model->getTimeZone(),

				),

			));
			?>
		</div>
		<div class="col-sm-6">
			<?php
			echo $form->dropDownListGroup($model,'pinned',array(
				'widgetOptions'=>array(
					'data'=>array('0'=>'No','1'=>'yes'),

				),

			));
			?>
		</div>
	</div>
	</div>
<!-- 	<div class="col-sm-6">
		<div class="col-sm-12"><h3 style="color:darkred">Travels posts Configurations</h3></div>
		<div class="col-sm-12">
			<a href="#"  onclick="guiders.show('fifth');return false"    ><i class="fa fa-question-circle"></i></a>

			<?php
/*			echo $form->dropDownListGroup($model,'jobs_posts_per_day',array(
				'widgetOptions'=>array(
					'data'=>$model->all_numbers(),

				),

			));
			*/?>
		</div>




		<div class="col-sm-12">
			<a href="#"  onclick="guiders.show('six');return false"    ><i class="fa fa-question-circle"></i></a>

			<?php
/*			echo $form->dropDownListGroup($model,'jobs_posts_per_week',array(
				'widgetOptions'=>array(
					'data'=>$model->posts_per_week(),

				),

			));
			*/?>
		</div>

	</div>-->
	<div class="col-sm-12">

		<div class="form-actions  pull-right" style="margin-bottom: 20px;margin-right:15px;">
			<?php $this->widget(
				'booster.widgets.TbButton',
				array(
					'buttonType' => 'submit',
					'context' => 'primary',
					'label' =>'Save',
				)
			);
			?>
		</div>
	</div>
	<script>
		$( document ).ready(function() {
			$('.StartTime').datetimepicker({
				datepicker:false,
				format:'H:i:00',
				step:30
			});
			$('.EndTime').datetimepicker({
				datepicker:false,
				format:'H:i:00',
				step:30
			});
		});
	</script>
<?php $this->endWidget(); ?>

</div><!-- form -->
<!--Tour -->
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=sixteen'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'firstSettings',
		'title'        => 'Posts per day',
		'next'         => 'second',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'assign max number of posts to posted per day',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Settings_how_many_posts',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=sixteen'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'second',
		'title'        => 'Time Gap',
		'next'         => 'third',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Time gap between every post',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Settings_gap_time',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=sixteen'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'third',
		'title'        => 'Start',
		'next'         => 'fourth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Time to start post to platforms ',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Settings_start_date_time',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=sixteen'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fourth',
		'title'        => 'End',
		'next'         => 'fifth',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'Time to end post to platforms ',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Settings_end_date_time',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$createLink = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=sixteen'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'fifth',
		'title'        => 'Jobs Posts per day',
		'next'         => 'six',

		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?
		'description'   => 'How many these posts will be posted per day',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Settings_jobs_posts_per_day',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>
<?php
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=seventeen'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'six',
		'title'        => 'Gap between posting days',


		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'Gap between each job post per week',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Settings_jobs_posts_per_week',
		'position'      => 1,
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
		'closeOnEscape' => true,
	)
);
?>

<?php
$continueTour = Yii::app()->createUrl('postQueue/main',array('#' => 'guider=postQueueTour'));
$this->widget('ext.eguiders.EGuider', array(
		'id'           => 'SettingsTour',
		'title'        => 'Settings',

		'buttons'      => array(
			array(
				'name'   => 'Previous',
				'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('first');}"
			),
			array('name'=>'Next ','classString' => 'tourcolor','onclick'=> "js:function(){  document.location = '$continueTour';}"),



			array(
				'name'   => 'Exit',
				'onclick'=> "js:function(){guiders.hideAll();}"
			)
		),
		// why not call renderPartial to get the content of the Guide ? .. yeah, why not ?

		'description'   => 'Go to settings to choose your preferred posting patterns, and the website sections you wish to post from\ focus on. Sortechs saves your settings, so you will only need to do this once. You can always go back and change your settings anytime you want, to suit your needs.',
		'overlay'       => true,
		// you can attach your guide to any element in the page thanks to JQuery selectors
		'attachTo'      => '#Settingstour',
		'position'      => 7,
		'classString' 	=> 'custom',
	'isHashable'    => true,
		'cssFile'     => Yii::app()->baseUrl. '/css/custom_guiders.css',
		'xButton'       => true,
		'onShow'        => 'js:function(){ $(".highlight pre").show();}',
	'autoFocus'      => true,
		'closeOnEscape' => true,
	)
);
?>

