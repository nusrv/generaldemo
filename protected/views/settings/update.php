<?php
/* @var $this SettingsController */
/* @var $model Settings */
$this->pageTitle = "Settings | Update";

$this->breadcrumbs = array(
    'Settings' => array('settings/update/1'),
    'Update',
);
?>
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-info">
                <div class="box-header with-border">

                    <div class="col-md-6 pull-left" style="text-align: left;">
                        <?php echo CHtml::link('Manage  custom settings ',array('admin'),array('url'=>'123','class'=>'btn btn-primary btn-sm pull-left')) ?>
                    </div>
                    <div class="col-md-6 pull-right" style="text-align: left;">
                        <?php echo Yii::app()->params['statement']['previousPage']; ?>
                    </div>
                </div>
                <div class="box-body">
                    <?php
                    if (isset($_POST['submitted'])) {
                        ?>
                        <div class="alert alert-info">
                            Updated
                        </div>
                        <?php
                    }
                    ?>
                    <?php $this->renderPartial('_form', array('model' => $model)); ?>
                </div>
            </div>
        </div>
    </div>
</section>

