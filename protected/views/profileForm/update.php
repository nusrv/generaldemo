<?php
/* @var $this ProfileFormController */
/* @var $model ProfileForm */



?>
<script>
	$(document).ready(function(){
		$('.passwords').hide();
		$('#passwordChanger').click(function(){
			if($('.passwords').is(":visible")){
				$('.passwords').hide('slow');
				$(this).html('Change password');
				$('#User_new_password').val("");
				$('#User_password').val("");
				$('#User_confirm_password').val("");


			}else{
				$('.passwords').show('slow');
				$(this).html('Hide password');



			}

		})
	});
</script>
<?php
if(Yii::app()->user->id != $model->id){
?>
<section class="content">
	<div class="row">
		<div class="box box-info">
			<div class="box-header with-border">

				<div class="col-xs-12  alert alert-danger">
					<div class="col-xs-6  pull-left">
						<h2>Invalid request</h2>
					</div>


				</div>
			</div>
		</div>
	</div>
</section>
<?php
}else { ?>
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<?php
						if (isset($_POST['updated'])) {
							echo '<div class="alert alert-info">Updated</div>';
						}
						?>
						<div class="col-sm-9"><h2>
								<?php $this->widget(
									'booster.widgets.TbButtonGroup',
									array(
										'size' => 'small',
										'context' => 'info',
										'buttons' => array(
											array(
												'label' => 'Update password',
												'buttonType' => 'link',
												'url' => array('/profileForm/updatePassword', 'id' => Yii::app()->user->id)
											),
										),
									)
								); ?></h2></div>

					</div>
					<div class="box-body">
						<?php $this->renderPartial('_form', array('model' => $model)); ?>
					</div>
				</div>
			</div>
	</section>
	<?php
}
?>