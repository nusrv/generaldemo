<?php

/**
 * This is the model class for table "post_template".
 *
 * The followings are the available columns in table 'post_template':
 * @property integer $id
 * @property string $type
 * @property string $text
 * @property integer $platform_id
 * @property integer $deleted
 * @property integer $catgory_id

 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property Platform $platform
 * @property Category $category
 */
class PostTemplate extends BaseModels
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post_template';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text, created_at', 'required'),
			array('platform_id', 'check_submitted'),
			array('platform_id', 'check_submitted_instagram'),
			array('platform_id', 'check_twitter_preview'),
			array('platform_id, deleted,catgory_id', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>14),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, text, platform_id,catgory_id, deleted, created_at,pagination_size', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'platform' => array(self::BELONGS_TO, 'Platform', 'platform_id'),
			'category' => array(self::BELONGS_TO, 'Category', 'catgory_id'),

		);
	}
	public function check_submitted($att){
		if($this->platform_id==0 and $this->type != 'image'){

				$this->addError('platform_id', 'Instagram only accept image');

		}
	}
	public function check_submitted_instagram($att){
		if($this->platform_id==3 and $this->type != 'image'){

			$this->addError('platform_id', 'Instagram only accept image');

		}
	}
	public function check_twitter_preview($att){
		if($this->platform_id==2 and $this->type == 'preview'){

			$findme   = '[short_link]';
			$pos = strpos($this->text, $findme);

			if(!$pos) {
				$this->addError('platform_id', 'You have chosen preview on twitter this  require short link');
			}
		}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$array= array(
			'id' => 'ID',
			'type' => 'Type',
			'text' => 'Template',
			'platform_id' => 'Platform',
			'catgory_id' => 'Section',
			'deleted' => 'Deleted',
			'created_at' => 'Created At',
			'visible_text' => 'Text',
		);
		return array_merge($this->labels(),$array);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		if(isset($this->pagination_size))
			$pages = $this->pagination_size;
		else
			$pages=5;
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('platform_id',$this->platform_id);
		$criteria->compare('catgory_id',$this->catgory_id);
		$criteria->compare('deleted',$this->deleted);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'pagination' => array('pageSize' => $pages),
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PostTemplate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}
