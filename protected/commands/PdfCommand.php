<?php
class PdfCommand extends BaseCommand{

    public function run($args){
        $this->TimeZone();
        $Pdf = new Pdf();
        $Pdf = $Pdf->get_not_generated();

        foreach ($Pdf as $item) {
            $this->generate($item);
            $item->generated = 1;
            if($item->save()){
                $emails = Emails::model()->findAll();
                if(!empty($emails)) {
                    foreach ($emails as $email) {
                        $this->send_Pdf_email('Sortechs - System', $email->f_name,$email->l_name, $email->email, 'This is PDF file', 'Your PDF file', $item->media_url,$item->from_date,$item->to_date);
                    }
                }
            }else {
                $this->send_email($item, 'error on PDF');
            }
        }

    }


    public function generate($model){


        $from_date = date_create($model->from_date);
        $from_date = date_format($from_date,'Y-m-d 00:00:00');
        $to_date = date_create($model->to_date);
        $to_date = date_format($to_date,'Y-m-d 23:59:59');
        $criteria=new CDbCriteria;
        $criteria->condition = "schedule_date >= '". $from_date."' and schedule_date <= '".$to_date."' and is_scheduled=1 and parent_id is null GROUP BY news_id order by schedule_date";
        $data = PostQueue::model()->findAll($criteria);
        $criteria->condition = "schedule_date >= '".$from_date."' and schedule_date <= '".$to_date."' and is_scheduled=1 order by schedule_date";
        $data_parent = PostQueue::model()->findAll($criteria);
        $file = Yii::app()->params['webroot'].'/uploads/temp/';
        if (!file_exists($file) AND !is_dir($file))
            mkdir($file);

        $html = $file.time().'.html';
        $pdf  = $file.time().'.pdf';

        $handle  = fopen($html, 'w') or die('Cannot open file:  '.$html); //implicitly creates

        $ccc = new BaseController('context');

        fwrite($handle,  $ccc->renderInternal(
            Yii::getPathOfAlias('application.views.pdf').'/file.php',
            array("pdf"=>true,'model'=>$data,	'model_parent'=>$data_parent,),
            true
        )
        );
        Yii::import('application.vendors.phpwkhtmltopdf.WkHtmlToPdf.php');
        $options = array(
            'encoding' => 'UTF-8',
            'margin-right'  => 0,
            'margin-left'   => 0,
            'orientation' => 'landscape',
            'footer-spacing' => 2,
            'page-size'     => 'A3'
        );
        $pdfW = new WkHtmlToPdf;
        $pdfW->setOptions($options);
        $pdfW ->ignoreWarnings = true;
        $pdfW->addPage($html);
        if(!$pdfW->saveAs($pdf))
            throw new Exception('Could not create PDF: '.$pdfW->getError());
        if(isset(explode(Yii::app()->params['webroot'],$pdf)[1])){
            $pdf =explode(Yii::app()->params['webroot'],$pdf)[1];
        }

        $p =  $this->upload_pdf($pdf);
        if(!empty($p)){
            unlink($html);
            unlink(Yii::app()->params['webroot'].$pdf);
            $model->media_url =$p;
            $model->generated = true;
            $model->command = false;
            if(!$model->save(true))
                $this->send_email($model,'error on PDF');
        }else{
            unlink( $html);
            unlink(Yii::app()->params['webroot'].$pdf);
        }
    }

}