<?php
class JobCommand extends BaseCommand{

    private $per_day = 0;
    private $count_per_day = 0;
    public function run($args){
        $this->TimeZone();
        $setting = Settings::model()->findByPk(1);
        $date =  date('Y-m-d',strtotime($setting->date_check_job.' + '.$setting->jobs_posts_per_week.' day'));
        if(date('Y-m-d') >= $date  ){
            $this->per_day = $setting->jobs_posts_per_day;
            $this->generator() ;
            $setting->date_check_job = date('Y-m-d');
            $setting->command= false;
            if(!$setting->save())
                $this->send_email($setting,'error on job settings');


        }
    }

    public function generator(){

        echo 'generator()'.PHP_EOL;


        $criteria=new CDbCriteria;
        $criteria->condition = "DATE_FORMAT(start_date,'%Y-%m-%d') <= '".date('Y-m-d')."' and DATE_FORMAT(end_date,'%Y-%m-%d') >= '".date('Y-m-d')."' and DATE_FORMAT(last_generated,'%Y-%m-%d') != '".date('Y-m-d')."' and catch=0 ORDER BY RAND()";
        $Jobs = Jobs::model()->findAll($criteria);
        if(!empty($Jobs)){
            foreach ($Jobs as $Job) {
                $Job->catch = 1;
                $Job->last_generated = date('Y-m-d');
                $Job->command= false;
                if(!$Job->save())
                    $this->send_email($Job,'error on job ');

            }
            $this->count_per_day ++;

            $is = $this->per_day <= $this->count_per_day ?1:0;
            echo $is.PHP_EOL;

            foreach ($Jobs as $Job) {

                $PostQueue = new PostQueue();
                $PostQueue->setIsNewRecord(true);
                $PostQueue->id= null;
                $PostQueue->command= false;
                $PostQueue->type = $Job->type;
                $PostQueue->post = $Job->text;
                $PostQueue->schedule_date = date('Y-m-d '.$Job->publish_time);
                $PostQueue->catgory_id = null;
                $PostQueue->media_url = $Job->media_url;
                $PostQueue->link = $Job->link;
                $PostQueue->is_posted = 0;
                $PostQueue->news_id =null;
                $PostQueue->post_id =null;
                $PostQueue->is_scheduled =$is;
                $PostQueue->platform_id =$Job->platform_id;
                $PostQueue->generated ='manual';
                $PostQueue->created_at =date('Y-m-d H:i:s');
                if(!$PostQueue->save())
                    $this->send_email($PostQueue,'error on job post queue');


            }




        }else{
            $Jobs = Jobs::model()->findAll();
            foreach ($Jobs as $Job) {
                $Job->catch = 0;
                $Job->last_generated =date('Y-m-d',strtotime(date('Y-m-d').' -3 day'));
                $Job->command= false;
                $Job->save();

            }
            $this->generator();

        }


    }
}