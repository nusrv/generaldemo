<?php
class CategoryCommand extends BaseCommand{

    public function run($args){
        $html = new SimpleHTMLDOM();
        $html_url = $html->file_get_html(Yii::app()->params['feedUrlRss']);
        $html_categories = $html_url->find('div [class=sitemapbox] ul li[class=sitemap_top]');
        foreach ($html_categories as $html_category) {
            $rss_url = Yii::app()->params['feedUrl'].$html_category->first_child()->href;
            $title = trim($html_category->first_child()->plaintext);
            $title_url = str_replace(array(' '),'',$title);
            $title_url = str_replace(array('&'),'-',$title_url);
            $url = Yii::app()->params['feedUrl'].'/'.strtolower($title_url);
            $section = Category::model()->findByAttributes(array('url_rss'=>$rss_url));
            if($title != 'Full Site RSS' and $title != 'Blogs'){
                if(empty($section)){
                    $section = new Category();
                    $section->id = null;
                    $section->setIsNewRecord(true);
                    $section->title = $title;
                    $section->url = $url;
                    $section->url_rss = $rss_url;
                    $section->active = 1;
                    $section->deleted = 0;
                    if(!$section->save(false))
                        print_r($section->getErrors());
                }
                if(!empty($section)){
                    foreach($html_category->find('ul li a') as $one_sub){
                        $rss_url = Yii::app()->params['feedUrl'].$one_sub->href;
                        $title = $one_sub->plaintext;
                        $url = $rss_url;
                        $sub_section = SubCategories::model()->findByAttributes(array('url_rss'=>$rss_url));
                        if(empty($sub_section)){
                            $sub_section = new SubCategories();
                            $sub_section->id = null;
                            $sub_section->setIsNewRecord(true);
                            $sub_section->category_id = $section->id;
                            $sub_section->title = $title;
                            $sub_section->url = $url;
                            $sub_section->url_rss = $rss_url;
                            $sub_section->active = 1;
                            $sub_section->deleted = 0;
                            if(!$sub_section->save(false))
                                print_r($sub_section->getErrors());
                        }
                    }
                }
            }


        }

        /*foreach (Category::model()->findAllByAttributes(array('active'=>1,'deleted'=>0)) as $item) {
            if(!empty($item->Subcategory)){
                $html = new SimpleHTMLDOM();
                $html_url = $html->file_get_html($item->url);
                $html_categories = $html_url->find('div[class=submenu] ul li a');
                foreach ($html_categories as $html_category) {
                    $title =  $html_category->innertext;
                    $url =  Yii::app()->params['feedUrl'].$html_category->href;
                    $sub_section = SubCategories::model()->findByAttributes(array('title'=>$title));
                    if(!empty($sub_section)){
                        $sub_section->url = $url;
                        if(!$sub_section->save(false))
                            print_r($sub_section->getErrors());
                    }
                }
            }
        }*/
    }
}