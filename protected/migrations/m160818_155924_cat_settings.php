<?php

class m160818_155924_cat_settings extends CDbMigration
{
	public function up()
	{
	    $this->execute("ALTER TABLE `platform_category_settings` CHANGE `category_id` `category_id` INT(11) NULL DEFAULT NULL;");
	}

	public function down()
	{
		echo "m160818_155924_cat_settings does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}